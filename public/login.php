<?php
/***********************************************************
 * 后台入口文件
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
// [ 应用入口文件 ]
namespace think;

//检测PHP环境
if(version_compare(PHP_VERSION,'7.1.0','<'))  die('本系统要求PHP版本 >= 7.1.0，当前PHP版本为：'.PHP_VERSION . '，请到虚拟主机控制面板里切换PHP版本，或联系空间商协助切换。<a href="http://www.pcfcms.com" target="_blank">点击查看安装教程</a>');
error_reporting(E_ERROR | E_WARNING | E_PARSE);//报告运行时错误
// 入口目录
define('WWW_ROOT', dirname(__FILE__));
// 定义应用目录
define('ROOT_PATH',dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR);
// 插件目录
define('PLUGINS_PATH', ROOT_PATH .'plugin');
// 检测是否已安装PCFCMS系统
if(!file_exists(ROOT_PATH .'public/install/install.lock') && file_exists(ROOT_PATH .'public/install/')){
    header('Location:/install/');exit;
}
//前端缓存时间
define('HOME_CACHE_TIME', 86400);
//后端缓存时间
define('PCFCMS_CACHE_TIME', 86400);
require __DIR__ . '/../vendor/autoload.php';
// 执行HTTP应用并响应
$http = (new App())->http;
$response = $http->name('admin')->run();
$response->send();
$http->end($response);