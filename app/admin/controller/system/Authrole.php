<?php
/***********************************************************
 * 角色管理
 * @作者 pcfcms <1131680521@qq.com>
 * @版权 广州市春风科技有限公司
 * @主页 http://www.pcfcms.com
 * @时间 2019年12月21日
***********************************************************/
namespace app\admin\controller\system;

use think\facade\Db;
use think\facade\Session;
use think\facade\Request;
use app\admin\controller\Base;
use app\admin\model\AuthRole as RoleModel;

class Authrole extends Base
{
    public $popedom = '';
    public function initialize() {
        parent::initialize();
        $ctl_act = Request::controller().'/index';
        $this->popedom = appfile_popedom($ctl_act);
        
    }
    //用户组列表
    public function index(){
        //验证权限
        if(!$this->popedom["list"]){
            return $this->errorNotice(config('params.auth_msg.list'),true,3,false);
        }
        if (Request::isAjax()) {
            $RoleModel = new RoleModel();
            return $RoleModel->tableData(input('param.'));
        }
        return $this->fetch();
    }
    //添加用户组
    public function add()
    {
        $RoleModel = new RoleModel();
        if (Request::isPost()) {
            if(!$this->popedom["add"]){
                if(config('params.auth_msg.test')){
                    $result = ['code' => 1, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['code' => 1, 'msg' => config('params.auth_msg.add')];
                    return $result;                    
                }
            }
            return $RoleModel->toAdd(input('param.'));
        }
        //获取全部权限菜单
        $getauthmenulist = Db::name('menu')->where(['parent_id'=>0,'is_show'=>1])->column('*', 'id');
        foreach ($getauthmenulist as $key => $value) {
            $getauthmenulist[$key]['child'] = getchandList1($value['id']);
        }
        $role_id =  Session::get('admin_info.role_id');
        if($role_id > 0){
            $admin_popedom = Session::get('admin_info.auth_role_info.permission');
            $this->assign('admin_popedom',$admin_popedom);
        }else{
            $admin_popedom = -1;
            $this->assign('admin_popedom',$admin_popedom);
        }
        $this->assign('authmenu',$getauthmenulist);
        return $this->fetch();
    }
    //编辑用户组
    public function edit()
    {
        $id = input('param.id');
        $RoleModel = new RoleModel();
        if (Request::isPost()) {
            if(!$this->popedom["modify"]){
                if(config('params.auth_msg.test')){
                    $result = ['code' => 1, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['code' => 1, 'msg' => config('params.auth_msg.modify')];
                    return $result;                    
                }
            } 
            return $RoleModel->toAdd(input('param.'));
        }
        //获取全部权限菜单
        $getauthmenulist = Db::name('menu')->where(['parent_id'=>0,'is_show'=>1])->column('*', 'id');
        foreach ($getauthmenulist as $key => $value) {
            $getauthmenulist[$key]['child'] = getchandList1($value['id']);
        }
        $authinfo = Db::name('auth_role')->where('id',$id)->find();
        $this->assign("plist",unserialize($authinfo['permission']));
        $this->assign('authinfo',$authinfo);
        $role_id =  Session::get('admin_info.role_id');
        if($role_id > 0){
            $admin_popedom = 1;
        }else{
            $admin_popedom = -1;
        }
        $this->assign('admin_popedom',$admin_popedom);
        $this->assign('authmenu',$getauthmenulist);
        return $this->fetch();
    }
    //删除用户组
    public function del()
    {
        if (Request::isPost()) {
            if(!$this->popedom["delete"]){
                if(config('params.auth_msg.test')){
                    $result = ['code' => 1, 'msg' => config('params.auth_msg.pcfcms')];
                    return $result;
                }else{
                    $result = ['code' => 1, 'msg' => config('params.auth_msg.delete')];
                    return $result;                    
                }
            } 
            $id = input('post.id/d');
            //删除前判断是否有管理员关联
            if(Db::name('admin')->where("role_id",$id)->find()){
                $result = ['code' => 1, 'msg' => '请删除关联的管理员'];
                return $result; 
            }
            if(empty($id)){
                $result = ['code' => 1, 'msg' => '参数丢失'];
                return $result;  
            }
            if (Db::name('auth_role')->where("id",$id)->delete()) {
                $result = ['code' => 0, 'msg' => '删除成功'];
                return $result;
            } else {
                $result = ['code' => 1, 'msg' => '删除失败'];
                return $result;
            }
        }       
    }

}
