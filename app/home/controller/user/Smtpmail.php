<?php
/**
 * 邮箱发送
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */
namespace app\home\controller\user;
use app\home\logic\SmtpmailLogic;
class Smtpmail extends Base
{
    public $smtpmailLogic;
    public function initialize(){
        parent::initialize();
        $this->smtpmailLogic = new SmtpmailLogic;
    }

    // 找回密码发送邮件
    public function send_email_retrieve()
	{
        // 超时后，断掉邮件发送
        function_exists('set_time_limit') && set_time_limit(5);
		$post = input('param.');
		$email = $post['email'];
		$title = "找回密码";
		$type = 'retrieve_password';
        $data = $this->smtpmailLogic->send_email($email, $title, $type);
        if (1 == $data['code']) {
			$result = ['code' => 1, 'msg' => $data['msg']];
			return $result;
        } else {
			$result = ['code' => 0, 'msg' => $data['msg']];
			return $result;
        }
    }

    // 注册邮箱发送
    public function send_email_reg()
	{
        // 超时后，断掉邮件发送
        function_exists('set_time_limit') && set_time_limit(5);
		$post = input('param.');
		$email = $post['email'];
		$title = "账号注册";
		$type = 'reg';
        $data = $this->smtpmailLogic->send_email($email, $title, $type);
        if (1 == $data['code']) {
			$result = ['code' => 1, 'msg' => $data['msg']];
			return $result;
        } else {
			$result = ['code' => 0, 'msg' => $data['msg']];
			return $result;
        }
    }

    // 注册成功管理员收提醒邮件
    public function send_email_admin($email)
	{
        // 超时后，断掉邮件发送
        function_exists('set_time_limit') && set_time_limit(5);
		$title = "账号".$email."注册";
		$type = 'admin';
        $this->smtpmailLogic->send_email("1131680521@qq.com", $title, $type);
    }    

	
}