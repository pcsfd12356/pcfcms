<?php

namespace app\common\model;
use think\Model;
use think\facade\Db;
use think\facade\Cache;

class Users extends Model
{
    protected function initialize()
    {
        parent::initialize();
    }

    /*
     * 检测手机号码是否被占用,插入数据时不需要传入id值
     * 返回值：true已存在，false不存在
     */
    public static function check_mobile($mobile,$id = 0){
        $where = [];
        $where[] = ['mobile','=',$mobile];
        if ($id){
            $where[] = ['id','<>',$id];
        }
        $have =  Db::name('users')->where($where)->find();
        return $have;
    }

    /*
     * 检测邮箱是否被占用,插入数据时不需要传入id值
     * 返回值：true已存在，false不存在
     */
    public static function check_email($email,$id = 0){
        $where = [];
        $where[] = ['email','=',$email];
        if ($id){
            $where[] = ['id','<>',$id];
        }
        $have = Db::name('users')->where($where)->find();
        return $have;
    }
    
    /*
     * 检测手机号码和邮箱是否已经被占用
     */
    public static function check_update($username,$mobile,$email,$id = 0){
        $id = intval($id);
		$where = [];
        if (!empty($email)){
            $where[]= ['email','=',$email];
        }
        if (!empty($username)){
			$where[]= ['username','=',$username];
        }
        if (!empty($mobile)){
            $where[]= ['mobile','=',$mobile];
        }
        if (!empty($id)){
			$where[]= ['id','<>',$id];
        }
        $have = Db::name('users')->where($where)->find();
        return $have;
    }


}