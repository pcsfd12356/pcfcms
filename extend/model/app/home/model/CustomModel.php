<?php
/**
 * 自定义模型
 * ============================================================================
 * 网站地址: http://www.pcfcms.com
 * ----------------------------------------------------------------------------
 * 如果商业用途务必到官方购买正版授权, 以免引起不必要的法律纠纷.
 * ============================================================================
 * Author: 小潘 <1131680521@qq.com>
 * Date: 2019-12-21
 */

namespace app\home\model;
use think\facade\Db;
use think\facade\Cache;

class CustomModel
{
    protected function initialize()
    {
        parent::initialize();
    }

    // 获取单条记录
    public function getInfo($aid, $field = '', $isshowbody = true)
    {
        $data = array();
        if (!empty($field)) {
            $field_arr = explode(',', $field);
            foreach ($field_arr as $key => $val) {
                $val = trim($val);
                if (preg_match('/^([a-z]+)\./i', $val) == 0) {
                    array_push($data, 'a.'.$val);
                } else {
                    array_push($data, $val);
                }
            }
            $field = implode(',', $data);
        }
        $result = array();
        if ($isshowbody) {
            $field = !empty($field) ? $field : 'b.*, a.*';
            $result = Db::name('archives')->field($field)
                ->alias('a')
                ->join('custommodel_content b', 'b.aid = a.aid', 'LEFT')
                ->find($aid);
        } else {
            $field = !empty($field) ? $field : 'a.*';
            $result = Db::name('archives')->field($field)
                ->alias('a')
                ->where('a.aid',$aid)
                ->find();
        }
        return $result;
    }
}